function P=priority(c,w,skillvector,p)
%computes priority of challenge, the bigger the more important
%depends on functions: intensity, similarity, skillvector

%c=challenge vector to compute priority of  
%w= weight/trust between receiving agent a and sending agent 
%p= norm used

%skillvector= skillvector of agent a

P= norm(c,p)*w*similarity(skillvector,c);