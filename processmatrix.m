function [M,A] = processmatrix(n,density,x,p) 
%makes the average benefit extraction EQUAL to x

% n=dimension of wanted matrix
%density: percentage of non-zero elements in U ->still needs to be checked 
%out: density of U and M not the same, sparse matrix mostly det=0    density,
%x= factor by which a challenge on average is relaxed, ie new challenge is
%on average x*old challenge
%p= what kind of p-norm is used, ie p=1 is sum of absolute values, p=2 is
%standard euclidean norm,...

%all numbers or choosen randomly out of t-distribution with v=1, ie Cauchy
%distribution

 %U= sprandn(n,n,density); %choose eigenvectors

 
 M=sprandn(n,n,density);
 %[U,D]=eigs(M,n);
 [U,D]=eig(full(M));
 while det(U)==0 %chekc if eigenvectors are lin independent; if not, compute others
    M=sprandn(n,n,density);
 %[U,D]=eigs(M,n);
 [U,D]=eig(full(M));
 end
 
 
 V=inv(U);
 
 lambda=diag(D);
 
 norml=0;
 for k=1:n
     vector=0;
     for j=1:n
     vector=vector+lambda(j)*V(j,k)*U(:,j);
     end
 norml=norml+norm(vector,p);
 end

 lambda= norm(ones(1,n),p)*x*lambda/norml;   %make sure a challenge on average get relaxed by at least a factor x, by
          %normalising
 
          
 A=diag(lambda); %make a diagonal matrix from the eigenvalues
 M=sparse(real(U*A/U));  
 
 
 %5/11 : mostly OK for p=2: ok if x in breuk (bv 3/4), not if in number (bv
 %0.75). Also ok for x=1 or 0.5. Density=0.2, at end density=0.72
 %: 0.11 with approx 0.0001