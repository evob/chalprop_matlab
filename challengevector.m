function [c,y]= challengevector(N,a,cst,x,percneg)
%creates a challengevector out of a power-law with factor a, but if a
%component is smaller to a certain cst, make it zero. Choose b such that
%only x% of the components are non-zero

%c=created challengevector
%y= number of zero components

%a=2;
%cst=0.5;
%x=5;
b=-x/100*(-a+1)/(cst^(-a+1));
c= powerlaw(N,a,b,percneg);
y=0;
for i=1:N
    if abs(c(i))<cst
        c(i)=0;
        y=y+1;
    end
end
    
        