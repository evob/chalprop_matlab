print(figure(1), '-djpeg', ['avrelax','basic']);
print(figure(2), '-djpeg', ['chalintensity','basic']);
print(figure(3), '-djpeg', ['hypben','basic']);
print(figure(4), '-djpeg', ['totalposben','basic']);
print(figure(5), '-djpeg', ['totalben','basic']);
print(figure(6), '-djpeg', ['avpriority','basic']);


%basic= following parameters:
%processstyle=0;              %0 if the processmatrix is diagonalmatrix, 1 if it is a sparse matrix, 2/other if it is a full matrix 
%sendstyle=3;                 %0 if always send, 1 if with link weight as probability (and no pref function), 2 if with preference function, 3/other if with pref function and weight as prob
%updatestyle=0;                 %0 if the weights get renormalized, 1/other if we use decay (choose decay=0 if weights get renormalized).
%punishstyle=0;                %0 if neg benefit is used to lower the weights in between, 1 if not

% %parameters of model
% buffersize=5;         
% N=10;                 %number of components
% r=5;                 %number of rival components
% n=100;                 %number of agents
% 
% believe=0.1;               %percentage of agents getting god challenges in one iteration 0.2
% 
% T=100;                %number of iterations/time
% p=1;                 % norm used. Check if compatible: processmatrix works for norm 1, but not 2
% 
% %W=0.8;
% W=cell(T+1,1);
% W{1}=(ones(n,n)-eye(n))*1/n;  %initial matrix of network (0.1?)
% lambda=0.3;          %learning rate of network, between 0 and 1
% kappa=0.2;           %reciprocity rate, perc of link adaption also dapted in other direction, between 0 and 1
% decay=0;               %rate for which links are weakened in each step (0.01?), rate of disepation  -Should be 0 if we renormalize
% 
% ws=1;       %weightstrong, the weight is powered with this value to compute priority, so that the weight has less influence if ws<1
% 
% 
% factor=2;                 %factor of power-law for needvector                             
% cst=0.05;             %value under which components are zero in needvector
% x=50;                % x% of the components are non-zero in needvector
% 
% 
% factorsit=2;           %factor of power-law for situationvector 
% cstsit=0.01;              %value under which components are zero in situationvector
% xsit=25;                 % x% of the components are non-zero in situationvector
% 
% density=0.5;                  %density/percent of non-zero elements of processmatrix
% relax=0.5;                 %factor by which a challenge on average is relaxed
% 
% percsent=0.4;