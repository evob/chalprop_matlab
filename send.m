function input =send(situation,memory,a,W,need,skillvector,input,n,r,ws,RJ,p)

%situation=situation send
%memory=memory of all agents
%a=agent who send
%W=matrix of network
%need=matrix of needvectors
%skillvector=skillvector of a
%input= cell array of input-matrices for each agent
%n=number of agents
%r=number of rival components
%p= p-norm used

nr=nnz(W(a,:));   %number of non-zero elements, thus number of neighbours send to.

if rand(1)<RJ   %do random jump
    tmp=randperm(n);
    nb=tmp(1);
    nr=nr+1;
    
    challenge= situation(2:end) - need(nb,:);
        if sum(situation(1)==memory{nb}) ~= 0  %if challenge has allready passed, non-rival parts are ignored
           c=[challenge(1:r)/nr zeros(1,length(challenge)-r)];
        else
            c=[challenge(1:r)/nr challenge(r+1:end)];
        end
        pr=priority(c,W(a,nb)^(ws),skillvector,p);
        input{nb}=[input{nb}; pr a situation(1) challenge];
end

for j=find(W(a,:))      
    
        challenge= situation(2:end) - need(j,:);
        if sum(situation(1)==memory{j}) ~= 0  %if challenge has allready passed, non-rival parts are ignored
           c=[challenge(1:r)/nr zeros(1,length(challenge)-r)];
        else
            c=[challenge(1:r)/nr challenge(r+1:end)];
        end
        pr=priority(c,W(a,j)^(ws),skillvector,p);
        input{j}=[input{j}; pr a situation(1) challenge];                      
   
end