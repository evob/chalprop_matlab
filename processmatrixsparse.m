function [M,A] = processmatrixsparse(n,density,x,p) 
%makes the average benefit extraction EQUAL to x

% n=dimension of wanted matrix
%density: percentage of non-zero elements in U ->still needs to be checked 
%out: density of U and M not the same, sparse matrix mostly det=0    density,
%x= factor by which a challenge on average is relaxed, ie new challenge is
%on average x*old challenge
%p= what kind of p-norm is used, ie p=1 is sum of absolute values, p=2 is
%standard euclidean norm,...

%all numbers or choosen randomly out of t-distribution with v=1, ie Cauchy
%distribution

 %U= sprandn(n,n,density); %choose eigenvectors

 M=diag(randn(1,n));
 tmp=randperm(n);
 M=M(tmp,:);
 
 for i=1:n
  zeropos=find(M(:,i)==0);   
  nrz=length(zeropos);   %number of zeros
  M(zeropos,i)=sprandn(nrz,1,(n*(density-1)+nrz)/nrz);   
 end
 
 
 
 %[U,D]=eigs(M,n);
 [U,D]=eig(full(M));
 if det(U)==0 %chekc if eigenvectors are lin independent; if not, compute others
    error('determinant is still 0');
 end
 
 
 V=inv(U);
 
 lambda=diag(D);
 
 norml=0;
 for k=1:n
     vector=0;
     for j=1:n
     vector=vector+lambda(j)*V(j,k)*U(:,j);
     end
 norml=norml+norm(vector,p);
 end

 lambda= norm(ones(1,n),p)*x*lambda/norml;   %make sure a challenge on average get relaxed by at least a factor x, by
          %normalising
 
          
 A=diag(lambda); %make a diagonal matrix from the eigenvalues
 M=sparse(real(U*A/U));  
 M=approx(M,0.001);
 
 
 %5/11 : mostly OK for p=2: ok if x in breuk (bv 3/4), not if in number (bv
 %0.75). Also ok for x=1 or 0.5. Density=0.2, at end density=0.72
 %: 0.11 with approx 0.0001