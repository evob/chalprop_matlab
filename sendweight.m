function input =sendweight(situation,memory,a,W,need,skillvector,input,n,r,RJ,p)

%situation=situation send
%memory=memory of all agents
%a=agent who send
%W=matrix of network
%need=matrix of needvectors
%skillvector=skillvector of a
%input= cell array of input-matrices for each agent
%n=number of agents
%r=number of rival components
%p= p-norm used

reachedagents=[];
nr=0;

if rand(1)<RJ   %do random jump
    tmp=randperm(n);
    nb=tmp(1);
    reachedagents=nb;
    nr=1;      
end

for j=find(W(a,:))
    if rand(1)<W(a,j)
        reachedagents=[reachedagents j];
        nr=nr+1;
    end
end

if nr~=0
for j=reachedagents
        challenge= situation(2:end) - need(j,:);
        if sum(situation(1)==memory{j}) ~= 0  %if challenge has allready passed, non-rival parts are ignored
           c=[challenge(1:r)/nr zeros(1,length(challenge)-r)];
        else
            c=[challenge(1:r)/nr challenge(r+1:end)];
        end
        pr=priority(c,1,skillvector,p);
        input{j}=[input{j}; pr a situation(1) challenge];                      
    
end
end