function b= benefit(c,M)

%c= challenge vector
% M= processmatrix

b=norm(c,1)-norm(M*c,1);