function input =sendpref(situation,memory,preference,a,W,need,skillvector,input,n,r,percsent,sendstyle,ws,RJ,p)

%situation=situation send
%memory=memory of all agents
%preference= vector with prefence vectors of agents, from agent a
%a=agent who send
%W=matrix of network
%need=matrix of needvectors
%skillvector=skillvector of a
%input= cell array of input-matrices for each agent
%n=number of agents
%r=number of rival components
%p= p-norm used


D=ones(size(W));
E=ones(size(W));
if sendstyle==2    % weight in intensity
    D=W;
else              %weight as probability/part of sending chance
    E=W;
end

pref=zeros(nnz(W(a,:)),2+size(preference,2));

nze=find(W(a,:));  %nonzero elements
for k=1:nnz(W(a,:))
        j=nze(k);
        pref(k,:)=[norm(situation(2:end),p)*E(a,j)*similarity(preference(j,:),situation(2:end)) j preference(j,:)];    
end

if isempty(pref)==0
%pref=sortrows(pref,1);
[~,indx]=sort(pref(:,1));
        pref=pref(indx,:);
end

nr=size(pref,1)- max(1,round((1-percsent)*size(pref,1)))+1;   %nr of neighbours send to (rival components are split up among them)

 if rand(1)<RJ   %do random jump
    tmp=randperm(n);
    nb=tmp(1);
    nr=nr+1;
    
    challenge= situation(2:end) - need(nb,:);
        if sum(situation(1)==memory{nb}) ~= 0  %if challenge has allready passed, non-rival parts are ignored
           c=[challenge(1:r)/nr zeros(1,length(challenge)-r)];
        else
            c=[challenge(1:r)/nr challenge(r+1:end)];
        end
        pr=priority(c,D(a,nb)^(ws),skillvector,p);
        input{nb}=[input{nb}; pr a situation(1) challenge];
end

for i= max(1,round((1-percsent)*size(pref,1))) : size(pref,1)
        j=pref(i,2);
        challenge= situation(2:end) - need(j,:);
        if sum(situation(1)==memory{j}) ~= 0  %if challenge has allready passed, non-rival parts are ignored
           c=[challenge(1:r)/nr zeros(1,length(challenge)-r)];
        else
            c=[challenge(1:r)/nr challenge(r+1:end)];
        end
        pr=priority(c,D(a,j)^(ws),skillvector,p);
        input{j}=[input{j}; pr a situation(1) challenge];                      
    
end