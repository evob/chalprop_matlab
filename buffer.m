function [B,sink,localbenefit]=buffer(input,buffer,sink,buffersize,n)
%depends on function priority
%input=matrix of new challenges, with the first element in each row (=each
%challenge) the priority , ie structure of row is [priority(c), b, c] with
%b the agent from which the agent came

%  if isempty(buffer) ==0          %if buffer is not empty 
% B=sortrows(buffer,1);   %start with buffer, sort it, should be done allready
%  else
%      B=buffer;
%  end
%  
  B=buffer;
 localbenefit=zeros(n+1,1);    %the n+1th position is god (normally -1)
 
 
for i=1:size(input,1) % size(input,1)=number of new challenges
   
    
    if isempty(B) ==1       %if empty          
       
        B= [B; input(i,:)];
        B=sortrows(B,1);                 %sorted, lowest priority on top  
          for j=find(input(i,:)<0)                                   %punish for negative components  ALLWAYS OR IF SELECTED IN BUFFER?
        %if input(i,j)<0
            if input(i,2)==-1  %if god, at end
              localbenefit(n+1)=localbenefit(n+1)+input(i,j);
            else
              localbenefit(input(i,2))=localbenefit(input(i,2))+input(i,j);
            end
        %end
         end
         
    else
    if sum(input(i,3)==B(:,3))==0   %if new challenge not yet in buffer
    
    if size(B,1)<buffersize              %if buffer not full
        B= [B; input(i,:)];
        [~,indx]=sort(B(:,1));
        B=B(indx,:);
        %B=sortrows(B,1);                 %sorted, lowest priority on top
        
         for j=find(input(i,:)<0)                                  %punish for negative components  ALLWAYS OR IF SELECTED IN BUFFER?
       
            if input(i,2)==-1  %if god, at end
              localbenefit(n+1)=localbenefit(n+1)+input(i,j);
            else
              localbenefit(input(i,2))=localbenefit(input(i,2))+input(i,j);
            end
        
         end
    
    else                                %if full: add if higher priority then lowest
        
        
       if input(i,1)> B(1,1) % if priority(i) bigger then smallest priority in buffer
           sink= [sink; B(1,:)];        %put deleted challenge in sink          
            B(1,:)= input(i,:);        % change smallest-priority challenge by this one, ie del and add
           %B=sortrows(B,1);
           [~,indx]=sort(B(:,1));
           B=B(indx,:);
           
             for j=find(input(i,:)<0)                                  %punish for negative components  ALLWAYS OR IF SELECTED IN BUFFER?
        %if input(i,j)<0
            if input(i,2)==-1  %if god, at end
              localbenefit(n+1)=localbenefit(n+1)+input(i,j);
            else
              localbenefit(input(i,2))=localbenefit(input(i,2))+input(i,j);
            end
        %end
            end
    
       end
    end
    end
    end
end

           