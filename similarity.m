function s=similarity(v,w)
if v==zeros(1,length(v))
    v=ones(1,length(v));
end

if w==zeros(1,length(v))
    w=ones(1,length(v));
end

s=v*transpose(w)/(norm(v,2)*norm(w,2));
s=(s+1)/2;

%slower
% M=[v;w];
% s=pdist(M,'cosine');
% s=1-s/2;

