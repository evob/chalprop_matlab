%We want to combine different versions of our model, so that we can choose
%which one to use:
%D-we always take a percentage of god challenges, if all agents get a god
%challenge, this is 100%
%D-we always rescale simil so that it is between 0 and 1
%D-we can choose whether to use the link weight as prob or as part of the
%priority function  -->difference in send, priority. priority is used in
%godrain(weight is always 1 there, doesn't matter), (send)->ok, just use
%w=1 in sendweight, no other priority needed
%D-we can choose whether we work with a preference function or just send
%challenges to all neighbours
%D-we can choose whether we work with a diagonal process matrix, or just a sparse
%one (or a full one)     DONE
%-we should be able to choose whether links of neg benefit are punished
%-we should be able to choose between a fully connected (with small
%weights) network, or not connected, and allowing random jumps (always,
%might be decreasing in time)
%D-we should be able to choose whether we work with decay of weights, or
%renormalize weights at every step (which normally happens in neural
%networks) SHOULD BE IMPLEMENTED ->but weight in priority might be most
%logical, otherwise on average only send to 1 (always)
%-we should be able to choose whether, if working with a pref function, a
%certain percentage of neighbours is send to, or all challenges above
%certain treshold are send
%-transitivity learning
%
%We should keep track of more things:
%D-memory: also last visited agent: DONE, could be better (not increasing in
%every step)
%-situation vectors, not just challenge vectors (even tho they can be
%rebuild)
%-personal sinks? 
%D-network topology at all times
%-path of challenge (could be recontructed): as matrix, at every place
%weight= time step this link is passed (what if twice??????)
%
%We started with the code from Pmatrixdiag, this is withPreffunction (not
%with weight as prob)



%network, agent, situation vectors, god
%agent: needvector, processvector, buffer, selection mechanism

%version of model:
t=cputime;

  

processstyle=1;              %0 if the processmatrix is diagonalmatrix, 1 if it is a sparse matrix, 2/other if it is a full matrix 
sendstyle=3;                 %0 if always send, 1 if with link weight as probability (and no pref function), 2 if with preference function, 3/other if with pref function and weight as prob
updatestyle=1;                 %0 if the weights get renormalized, 1/other if we use decay (choose decay=0 if weights get renormalized).
punishstyle=0;                %0 if neg benefit is used to lower the weights in between, 1 if not
rjstyle=1;                    %0 if no random jumps, 1/other if (by chance 1/(1+r_0 sum(outweights) )
approxstyle=1;                %1 if matrix of graph is approxed (everything under treshold put to 0)

weight01=1;                   %if 0, weights can be bigger then 1 before renormalizing (only possible if updatestyle=0), other always between 0 and 1

%parameters of model
buffersize=10;         
N=10;                 %number of components
r=N/2;                 %number of rival components
n=100;                 %number of agents

believe=0.2;               %percentage of agents getting god challenges in one iteration 0.2
percneg=0.2;               %percentage of negative components in god challenges 

T=100;                %number of iterations/time
p=1;                 % norm used. Check if compatible: processmatrix works for norm 1, but not 2

%W=0.8;
W=cell(T+1,1);
W{1}= (ones(n,n)-eye(n))*1/n;  %initial matrix of network (0.1?)     sparse(zeros(n,n))  
lambda=0.1;          %learning rate of network, between 0 and 1   0.3
kappa=0;           %reciprocity rate, perc of link adaption also dapted in other direction, between 0 and 1      0.2
%decay=0.03;               %rate for which links are weakened in each step (0.01?), rate of disepation  -Should be 0 if we renormalize  0.05

ws=0.2;       %weightstrong, the weight is powered with this value to compute priority, so that the weight has less influence if ws<1


factor=2;                 %factor of power-law for needvector                             
cst=0.05;             %value under which components are zero in needvector
x=50;                % x% of the components are non-zero in needvector


factorsit=2;           %factor of power-law for situationvector 
cstsit=0.01;              %value under which components are zero in situationvector
xsit=25;                 % x% of the components are non-zero in situationvector

density=5/N;                  %density/percent of non-zero elements of processmatrix
relax=0.5;                 %factor by which a challenge on average is relaxed

percsent=0.4;   %0.4


r_0=99;  %thre bigger, the lesser chance on random jumps

%th=0.0010;

%initial conditions of agents
buff = cell(n, 1);      %create an empty buffer for each agent
skillvector=zeros(n,N);   %skillvector in beginning,  for all agents
benefit=zeros(T,n);       %benefit in beginning, no benefit yet as long as nothing happened
sink=zeros(1,N+3);

preference=cell(n,1);

testintermben=zeros(T,n);

intensity=zeros(T,n);
avintensity=zeros(1,T);
%preallocating
need=zeros(n,N);
P= cell(n,1);
memory=cell(n,1); %memory of received challenges, in the end aggregated with
memsa=cell(n,1); %memory of sending agents
rel=cell(n,1);
challengenorm=zeros(T,n);

localbenefit=zeros(n+1,n);

for a=1:n
need(a,:)= challengevector(N,factor,cst,x,percneg);             %needvector, for all a=agents
%processmatrix
if processstyle==0
P{a}=  processmatrixdiag(N,relax,p);               % processing matrix, for all a=agents   
else
    if processstyle==1
        P{a}=processmatrixsparse(N,density,relax,p);
    else
        P{a}=processmatrix3(N,relax,p);
    end    
end

preference{a}=zeros(n,N);
rel{a}=zeros(T,1);
end
mark=0; %0 challenges are created, thus marked, in the beginning

for i=1:T
    
    %if i==50   %from it 50 on, no-one believes anymore
     %   believe=0;
    %end
    
    input=cell(n,1);               % initially empty the inputs, no inputs yet
    W{i+1}=(1-decay)*W{i};           %all links are weakened. GOOD WAY TO IMPLEMENT?
    for a=1:n      %over all agents
        if isempty(buff{a}) ==0          %if buffer is not empty 
        challenge= buff{a}(end,3:size(buff{a},2)); %select challenge from buffer(agent)
        challengenorm(i,a)=norm(challenge(2:end),p);
        b=buff{a}(end,2);            %sending agent
        
        [newsituation,skillvector(a,:),benefit(i,a),memory{a},rel{a}(i)]= process(challenge,memory{a},P{a},need(a,:),skillvector(a,:),r,p); %process challenge (incl extract benefit, update skillvector)  (sometimes)
        memsa{a}=[memsa{a} b];
        %send newsituation                 (sometimes)
           
            %chance on random jumps
            
            if rjstyle==0
                RJ=0;
            else
                RJ=1/(1+r_0*sum(W{i}(a,:)));
            end
        
        
        if sendstyle==0
            input =send(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,ws,RJ,p);          
        else
            if sendstyle==1
            input =sendweight(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,RJ,p);
            else
            input =sendpref(newsituation,memory,preference{a},a,W{i},need,skillvector(a,:),input,n,r,percsent,sendstyle,ws,RJ,p);
              if b~=-1 
              preference{b}(a,:)=preference{b}(a,:)+(challenge(2:end)+need(a,:) );    %update preffunction sending agent, by adding sitvector SHOULD BE DONE AT END
              end
            end
        end
        
         
        testintermben(i,a)=benefit(i,a);
        if b~=-1      %if challenge doesn't come from god
            
                W{i+1}(b,a)=W{i+1}(b,a)+lambda*benefit(i,a);                      %network adaption
                W{i+1}(a,b)=W{i+1}(a,b)+kappa*lambda*benefit(i,a);     
            
        end
        
        else               %buffer empty
            memory{a}=[memory{a} 0];
            memsa{a}=[memsa{a} 0];
        end
        buff{a}=buff{a}(1:end-1,:);
    end
    %GOD challenges
    [input,mark] = godrain(N,n,factorsit,cstsit,xsit,need,skillvector,input,mark,believe,p,percneg);
    for a=1:n    %over all agents
        temp=buff{a};
        [buff{a},sink,localbenefit(:,a)]=buffer(input{a},temp,sink,buffersize,n); %select certain challenges from god, neighbours in buffer
    end
    benefit(i,a)=benefit(i,a)+sum(localbenefit(:,a));
    
    if punishstyle==0    %punish for neg challenges
        for j=1:n
            
             W{i+1}(j,a)=W{i+1}(j,a)+lambda*localbenefit(j,a);                      %network adaption
          W{i+1}(a,j)=W{i+1}(a,j)+kappa*lambda*localbenefit(j,a);   
            
        end
        
    end
    
    %update network
       W{i+1}=max(W{i+1},0); 
    
        if updatestyle==0
        for m=1:n
            if sum(W{i+1}(m,:))==0
                W{i+1}(m,:)=1/n;
            else
        W{i+1}(m,:)=W{i+1}(m,:)/sum(W{i+1}(m,:));
            end
        end
        end
       
            if weight01==0
                W{i+1}=max(W{i+1},0);                      
            else
                 W{i+1}=max(min(W{i+1},1),0);                             
            end
   
            if approxstyle==1
              W{i+1}=approx(W{i+1},th);
            end
   
    %transitivity learning
    
    for a=1:n
        if isempty(buff{a}) ==0          %if buffer is not empty
           intensity(i,a)=mean(buff{a}(:,1));
        else
           intensity(i,a)=0;
        end
    end
    avintensity(i)=sum(intensity(i,:));
end
totalposbenefit=sum(testintermben,2)/n;
totalbenefit=sum(benefit,2)/n;
Wapprox=cell(T+1,1);
for i=1:T+1
Wapprox{i}=approx(W{i},0.01);
end

for a=1:n
    memory{a}=[memory{a}; memsa{a}];
end

tijd=cputime-t;

%To be implemented:
%be sure no loops ->memory of agents, non-rival doesn't count if allready
%viewed:OK
%difference neg/pos ->punishment :+-OK:ALLWAYS OR IF SELECTED IN BUFFER?
%(now=if in buffer)
%buffer for outgoing sit's? ->somtimes send (dunno how it worked anymore)
%transitivity learning
%decay in links (vervaagd in tijd):OK

%Conceptual errors to fix:
%NaN in priority, is down by sorting, thus chosen (which means zero vectors
%are chosen). What is simil(zero vector, other)? Now: make it ones(...)
%be sure that not always god challenges chosen, just because weight=1
%for some reason all challenges not from god in buffer are same, even tho
%they have different marker (=ERROR). (in exp with n=1)
%also a lot of same challenges in sink, thus problem of generation/from sit
%to chal (in send or in process)