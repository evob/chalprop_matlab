

Fitlearn=[];
ax=fittype('a*x');
quad=fittype('a*x^2+b*x');

      for lambda=0:0.1:1
    for kappa=0:0.1:1
cd C:\Users\Evo\Documents\datasimul3-learningparam
load(['workspace',num2str(lambda),',',num2str(kappa)],'-mat','totalbenefit'); 


if sum(isnan(totalbenefit))==0       %if no NaN's in totalbenefit

 
f=fit( transpose(linspace(1,T,T)),totalbenefit,ax);
line=coeffvalues(f);

 g=fit( transpose(linspace(1,T,T)),totalbenefit,'poly1');
pol1=coeffvalues(g);

h=fit( transpose(linspace(1,T,T)),totalbenefit,'poly2');
pol2=coeffvalues(h);


k=fit( transpose(linspace(1,T,T)),totalbenefit,quad);
qu=coeffvalues(k);

else
line=NaN;
pol1=[NaN,NaN];
pol2=[NaN,NaN,NaN];
qu=[NaN,NaN];
end

Fitlearn= [Fitlearn;lambda,kappa, max(totalbenefit),line,pol1,pol2,qu];

    end
      end
    
       cd C:\Users\Evo\Dropbox\ECCO\MATLABcode\General-chooseoptions+moredata\code 

% [y,v]=max(Fit(:,8))          %calc max increasing in fit, the value and
% the place (by a*x)