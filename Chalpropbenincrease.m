function [negincrease,tijd] = Chalpropbenincrease(r)

%t=cputime;
tic

inputvector=  [0.9930    0.9487    0.7613    0.3515    0.4566];

believe=inputvector(1);
percneg=inputvector(2);
lambda=inputvector(3);
kappa=inputvector(4);
percsent=inputvector(5);

%version of model:

processstyle=1;              %0 if the processmatrix is diagonalmatrix, 1 if it is a sparse matrix, 2/other if it is a full matrix 
sendstyle=3;                 %0 if always send, 1 if with link weight as probability (and no pref function), 2 if with preference function, 3/other if with pref function and weight as prob
updatestyle=1;                 %0 if the weights get renormalized, 1/other if we use decay (choose decay=0 if weights get renormalized).
punishstyle=0;                %0 if neg benefit is used to lower the weights in between, 1 if not
rjstyle=1;                    %0 if no random jumps, 1/other if (by chance 1/(1+r_0 sum(outweights) )
approxstyle=1;                %1 if matrix of graph is approxed (everything under treshold put to 0)

weight01=0;                   %if 0, weights can be bigger then 1 before renormalizing (only possible if updatestyle=0), other always between 0 and 1

%parameters of model

buffersize=5;         
N=10;                 %number of components
%r=5;                 %number of rival components
n=100;                 %number of agents


T=100;                %number of iterations/time
p=1;                 % norm used. Check if compatible: processmatrix works for norm 1, but not 2

%W=0.8;
W=cell(T+1,1);
W{1}=sparse(zeros(n,n));  %initial matrix of network (0.1?)        (ones(n,n)-eye(n))*1/n

decay=0.05;               %rate for which links are weakened in each step (0.01?), rate of disepation  -Should be 0 if we renormalize  0.05

ws=0.2;       %weightstrong, the weight is powered with this value to compute priority, so that the weight has less influence if ws<1


factor=2;                 %factor of power-law for needvector                             
cst=0.05;             %value under which components are zero in needvector
x=50;                % x% of the components are non-zero in needvector


factorsit=2;           %factor of power-law for situationvector 
cstsit=0.01;              %value under which components are zero in situationvector
xsit=25;                 % x% of the components are non-zero in situationvector

density=1/N;                  %density/percent of non-zero elements of processmatrix
relax=0.7;                 %factor by which a challenge on average is relaxed




r_0=99;  %thre bigger, the lesser chance on random jumps

th=0.01;

%initial conditions of agents
buff = cell(n, 1);      %create an empty buffer for each agent
skillvector=zeros(n,N);   %skillvector in beginning,  for all agents
benefit=zeros(T,n);       %benefit in beginning, no benefit yet as long as nothing happened
sink=zeros(1,N+3);

preference=cell(n,1);

testintermben=zeros(T,n);

intensity=zeros(T,n);
avintensity=zeros(1,T);
%preallocating
need=zeros(n,N);
P= cell(n,1);
memory=cell(n,1); %memory of received challenges, in the end aggregated with
memsa=cell(n,1); %memory of sending agents
rel=cell(n,1);
challengenorm=zeros(T,n);

localbenefit=zeros(n+1,n);

for a=1:n
need(a,:)= challengevector(N,factor,cst,x,percneg);             %needvector, for all a=agents
%processmatrix
if processstyle==0
P{a}=  processmatrixdiag(N,relax,p);               % processing matrix, for all a=agents   
else
    if processstyle==1
        P{a}=processmatrixsparse(N,density,relax,p);
    else
        P{a}=processmatrix3(N,relax,p);
    end    
end

preference{a}=zeros(n,N);
rel{a}=zeros(T,1);
end
mark=0; %0 challenges are created, thus marked, in the beginning

for i=1:T
    
    %if i==50   %from it 50 on, no-one believes anymore
     %   believe=0;
    %end
    
    input=cell(n,1);               % initially empty the inputs, no inputs yet
    W{i+1}=(1-decay)*W{i};           %all links are weakened. GOOD WAY TO IMPLEMENT?
    for a=1:n      %over all agents
        if isempty(buff{a}) ==0          %if buffer is not empty 
        challenge= buff{a}(end,3:size(buff{a},2)); %select challenge from buffer(agent)
        challengenorm(i,a)=norm(challenge(2:end),p);
        b=buff{a}(end,2);            %sending agent
        
        [newsituation,skillvector(a,:),benefit(i,a),memory{a},rel{a}(i)]= process(challenge,memory{a},P{a},need(a,:),skillvector(a,:),r,p); %process challenge (incl extract benefit, update skillvector)  (sometimes)
        memsa{a}=[memsa{a} b];
        %send newsituation                 (sometimes)
           
            %chance on random jumps
            
            if rjstyle==0
                RJ=0;
            else
                RJ=1/(1+r_0*sum(W{i}(a,:)));
            end
        
        
        if sendstyle==0
            input =send(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,ws,RJ,p);          
        else
            if sendstyle==1
            input =sendweight(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,RJ,p);
            else
            input =sendpref(newsituation,memory,preference{a},a,W{i},need,skillvector(a,:),input,n,r,percsent,sendstyle,ws,RJ,p);
              if b~=-1 
              preference{b}(a,:)=preference{b}(a,:)+(challenge(2:end)+need(a,:) );    %update preffunction sending agent, by adding sitvector SHOULD BE DONE AT END
              end
            end
        end
        
         
        testintermben(i,a)=benefit(i,a);
        if b~=-1      %if challenge doesn't come from god
            
                W{i+1}(b,a)=W{i+1}(b,a)+lambda*benefit(i,a);                      %network adaption
                W{i+1}(a,b)=W{i+1}(a,b)+kappa*lambda*benefit(i,a);     
            
        end
        
        else               %buffer empty
            memory{a}=[memory{a} 0];
            memsa{a}=[memsa{a} 0];
        end
        buff{a}=buff{a}(1:end-1,:);
    end
    %GOD challenges
    [input,mark] = godrain(N,n,factorsit,cstsit,xsit,need,skillvector,input,mark,believe,p,percneg);
    for a=1:n    %over all agents
        temp=buff{a};
        [buff{a},sink,localbenefit(:,a)]=buffer(input{a},temp,sink,buffersize,n); %select certain challenges from god, neighbours in buffer
    end
    benefit(i,a)=benefit(i,a)+sum(localbenefit(:,a));
    
    if punishstyle==0    %punish for neg challenges
        for j=1:n
            
             W{i+1}(j,a)=W{i+1}(j,a)+lambda*localbenefit(j,a);                      %network adaption
          W{i+1}(a,j)=W{i+1}(a,j)+kappa*lambda*localbenefit(j,a);   
            
        end
        
    end
    
    %update network
       W{i+1}=max(W{i+1},0); 
    
        if updatestyle==0
        for m=1:n
            if sum(W{i+1}(m,:))==0
                W{i+1}(m,:)=1/n;
            else
        W{i+1}(m,:)=W{i+1}(m,:)/sum(W{i+1}(m,:));
            end
        end
        end
       
            if weight01==0
                W{i+1}=max(W{i+1},0);                      
            else
                 W{i+1}=max(min(W{i+1},1),0);                             
            end
   
            if approxstyle==1
              W{i+1}=approx(W{i+1},th);
            end
   
    %transitivity learning
    
    for a=1:n
        if isempty(buff{a}) ==0          %if buffer is not empty
           intensity(i,a)=mean(buff{a}(:,1));
        else
           intensity(i,a)=0;
        end
    end
    avintensity(i)=sum(intensity(i,:));
end
totalposbenefit=sum(testintermben,2)/n;
totalbenefit=sum(benefit,2)/n;

% Wapprox=cell(T+1,1);
% for i=1:T+1
% Wapprox{i}=approx(W{i},0.01);
% end

for a=1:n
    memory{a}=[memory{a}; memsa{a}];
end


ax=fittype('a*x'); 
f=fit( transpose(linspace(1,T,T)),totalbenefit,ax);
negincrease=-coeffvalues(f);

%tijd=cputime-t;
tijd=toc;

% if isnan(negincrease)==1     %if NaN, choose big enough number so that it won't be chosen
%     negincrease=9999999;     
% end
% 
% if negincrease==-Inf
%      negincrease=-9999999;     
% end
% 
% if negincrease==Inf
%      negincrease=9999999;     
% end
    