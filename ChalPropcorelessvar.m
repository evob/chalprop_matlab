%We want to combine different versions of our model, so that we can choose
%which one to use:
%D-we always take a percentage of god challenges, if all agents get a god
%challenge, this is 100%
%D-we always rescale simil so that it is between 0 and 1
%D-we can choose whether to use the link weight as prob or as part of the
%priority function  -->difference in send, priority. priority is used in
%godrain(weight is always 1 there, doesn't matter), (send)->ok, just use
%w=1 in sendweight, no other priority needed
%D-we can choose whether we work with a preference function or just send
%challenges to all neighbours
%D-we can choose whether we work with a diagonal process matrix, or just a sparse
%one (or a full one)     DONE
%-we should be able to choose whether links of neg benefit are punished
%-we should be able to choose between a fully connected (with small
%weights) network, or not connected, and allowing random jumps (always,
%might be decreasing in time)
%D-we should be able to choose whether we work with decay of weights, or
%renormalize weights at every step (which normally happens in neural
%networks) SHOULD BE IMPLEMENTED ->but weight in priority might be most
%logical, otherwise on average only send to 1 (always)
%-we should be able to choose whether, if working with a pref function, a
%certain percentage of neighbours is send to, or all challenges above
%certain treshold are send
%-transitivity learning
%
%We should keep track of more things:
%D-memory: also last visited agent: DONE, could be better (not increasing in
%every step)
%-situation vectors, not just challenge vectors (even tho they can be
%rebuild)
%-personal sinks? 
%D-network topology at all times
%-path of challenge (could be recontructed): as matrix, at every place
%weight= time step this link is passed (what if twice??????)
%
%We started with the code from Pmatrixdiag, this is withPreffunction (not
%with weight as prob)



%network, agent, situation vectors, god
%agent: needvector, processvector, buffer, selection mechanism

%version of model:

updatestyle=0;                 %0 if the weights get renormalized, 1/other if we use decay (choose decay=0 if weights get renormalized).
weight01=0;

%parameters of model
buffersize=5;         
N=10;                 %number of components
r=5;                 %number of rival components
n=100;                 %number of agents

T=100;                %number of iterations/time
p=1;                 % norm used. Check if compatible: processmatrix works for norm 1, but not 2

%W=0.8;
W=cell(T+1,1);
W{1}=(ones(n,n)-eye(n))*1/n;  %initial matrix of network (0.1?)
lambda=0.3;          %learning rate of network, between 0 and 1
kappa=0.2;           %reciprocity rate, perc of link adaption also dapted in other direction, between 0 and 1
decay=0;               %rate for which links are weakened in each step (0.01?), rate of disepation  -Should be 0 if we renormalize



factor=2;                 %factor of power-law for needvector                             
cst=0.05;             %value under which components are zero in needvector
x=50;                % x% of the components are non-zero in needvector


factorsit=2;           %factor of power-law for situationvector 
cstsit=0.01;              %value under which components are zero in situationvector
xsit=25;                 % x% of the components are non-zero in situationvector

density=0.5;                  %density/percent of non-zero elements of processmatrix


%initial conditions of agents
buff = cell(n, 1);      %create an empty buffer for each agent
skillvector=zeros(n,N);   %skillvector in beginning,  for all agents
benefit=zeros(T,n);       %benefit in beginning, no benefit yet as long as nothing happened
sink=zeros(1,N+3);

preference=cell(n,1);

testintermben=zeros(T,n);

intensity=zeros(T,n);
avintensity=zeros(1,T);
%preallocating
need=zeros(n,N);
P= cell(n,1);
memory=cell(n,1); %memory of received challenges, in the end aggregated with
memsa=cell(n,1); %memory of sending agents
rel=cell(n,1);
challengenorm=zeros(T,n);

localbenefit=zeros(n+1,n);


for a=1:n
need(a,:)= challengevector(N,factor,cst,x);             %needvector, for all a=agents
%processmatrix
if processstyle==0
P{a}=  processmatrixdiag(N,relax,p);               % processing matrix, for all a=agents   
else
    if processstyle==1
        P{a}=processmatrix(N,density,relax,p);
    else
        P{a}=processmatrix3(N,relax,p);
    end    
end

preference{a}=zeros(n,N);
rel{a}=zeros(T,1);
end
mark=0; %0 challenges are created, thus marked, in the beginning

for i=1:T
    
    %if i==50   %from it 50 on, no-one believes anymore
     %   believe=0;
    %end
    
    input=cell(n,1);               % initially empty the inputs, no inputs yet
    W{i+1}=(1-decay)*W{i};           %all links are weakened. GOOD WAY TO IMPLEMENT?
    for a=1:n      %over all agents
        if isempty(buff{a}) ==0          %if buffer is not empty 
        challenge= buff{a}(end,3:size(buff{a},2)); %select challenge from buffer(agent)
        challengenorm(i,a)=norm(challenge(2:end),p);
        b=buff{a}(end,2);            %sending agent
        
        [newsituation,skillvector(a,:),benefit(i,a),memory{a},rel{a}(i)]= process(challenge,memory{a},P{a},need(a,:),skillvector(a,:),r,p); %process challenge (incl extract benefit, update skillvector)  (sometimes)
        memsa{a}=[memsa{a} b];
        %send newsituation                 (sometimes)
        if sendstyle==0
            input =send(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,ws,p);          
        else
            if sendstyle==1
            input =sendweight(newsituation,memory,a,W{i},need,skillvector(a,:),input,n,r,p);
            else
            input =sendpref(newsituation,memory,preference{a},a,W{i},need,skillvector(a,:),input,n,r,percsent,sendstyle,ws,p);
              if b~=-1 
              preference{b}(a,:)=preference{b}(a,:)+(challenge(2:end)+need(a,:) );    %update preffunction sending agent, by adding sitvector SHOULD BE DONE AT END
              end
            end
        end
        
         
        testintermben(i,a)=benefit(i,a);
        if b~=-1      %if challenge doesn't come from god
            if weight01==0
                W{i+1}(b,a)=max(W{i+1}(b,a)+lambda*benefit(i,a),0);                      %network adaption
                W{i+1}(a,b)=max(W{i+1}(a,b)+kappa*lambda*benefit(i,a),0);     
            else
        W{i+1}(b,a)=max(min(W{i+1}(b,a)+lambda*benefit(i,a),1),0);                      %network adaption
        W{i+1}(a,b)=max(min(W{i+1}(a,b)+kappa*lambda*benefit(i,a),1),0);       
            end
        end
        
        else
            memory{a}=[memory{a} 0];
            memsa{a}=[memsa{a} 0];
        end
        buff{a}=buff{a}(1:end-1,:);
    end
    %GOD challenges
    [input,mark] = godrain(N,n,factorsit,cstsit,xsit,need,skillvector,input,mark,believe,p);
    for a=1:n    %over all agents
        temp=buff{a};
        [buff{a},sink,localbenefit(:,a)]=buffer(input{a},temp,sink,buffersize,n); %select certain challenges from god, neighbours in buffer
    end
    benefit(i,a)=benefit(i,a)+sum(localbenefit(:,a));
    
    if punishstyle==0
        for j=1:n
            if weight01==0
             W{i+1}(j,a)=max(W{i+1}(j,a)+lambda*localbenefit(j,a),0);                      %network adaption
          W{i+1}(a,j)=max(W{i+1}(a,j)+kappa*lambda*localbenefit(j,a),0);   
            else
          W{i+1}(j,a)=max(min(W{i+1}(j,a)+lambda*localbenefit(j,a),1),0);                      %network adaption
          W{i+1}(a,j)=max(min(W{i+1}(a,j)+kappa*lambda*localbenefit(j,a),1),0);
            end
        end
        
    end
    
    %update network
    if updatestyle==0
        for m=1:n
        W{i+1}(m,:)=W{i+1}(m,:)/sum(W{i+1}(m,:));
        end
    end
            
    %transitivity learning
    
    for a=1:n
        if isempty(buff{a}) ==0          %if buffer is not empty
           intensity(i,a)=mean(buff{a}(:,1));
        else
           intensity(i,a)=0;
        end
    end
    avintensity(i)=sum(intensity(i,:));
end
totalposbenefit=sum(testintermben,2)/n;
totalbenefit=sum(benefit,2)/n;
Wapprox=cell(T+1,1);
for i=1:T+1
Wapprox{i}=approx(W{i});
end

for a=1:n
    memory{a}=[memory{a}; memsa{a}];
end



%To be implemented:
%be sure no loops ->memory of agents, non-rival doesn't count if allready
%viewed:OK
%difference neg/pos ->punishment :+-OK:ALLWAYS OR IF SELECTED IN BUFFER?
%(now=if in buffer)
%buffer for outgoing sit's? ->somtimes send (dunno how it worked anymore)
%transitivity learning
%decay in links (vervaagd in tijd):OK

%Conceptual errors to fix:
%NaN in priority, is down by sorting, thus chosen (which means zero vectors
%are chosen). What is simil(zero vector, other)? Now: make it ones(...)
%be sure that not always god challenges chosen, just because weight=1
%for some reason all challenges not from god in buffer are same, even tho
%they have different marker (=ERROR). (in exp with n=1)
%also a lot of same challenges in sink, thus problem of generation/from sit
%to chal (in send or in process)