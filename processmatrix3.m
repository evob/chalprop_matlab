function [M,A] = processmatrix3(n,x,p) 
%makes the average benefit extraction EQUAL to x

% n=dimension of wanted matrix
%density: percentage of non-zero elements in U ->still needs to be checked 
%out: density of U and M not the same, sparse matrix mostly det=0    density,
%x= factor by which a challenge on average is relaxed, ie new challenge is
%on average x*old challenge
%p= what kind of p-norm is used, ie p=1 is sum of absolute values, p=2 is
%standard euclidean norm,...

%all numbers or choosen randomly out of t-distribution with v=1, ie Cauchy
%distribution

 %U= sprandn(n,n,density); %choose eigenvectors
 
  U=trnd(1,n,n);
 while det(U)==0 %chekc if eigenvectors are lin independent; if not, compute others
     U=trnd(1,n,n);
    %  U= sprandn(n,n,density);
 end
 
 
 V=inv(U);
 
 lambda=trnd(1,n,1);  %choose eigenvalues
 
 norml=0;
 for k=1:n
     vector=0;
     for j=1:n
     vector=vector+lambda(j)*V(j,k)*U(:,j);
     end
 norml=norml+norm(vector,p);
 end

 lambda= norm(ones(1,n),p)*x*lambda/norml;   %make sure a challenge on average get relaxed by at least a factor x, by
          %normalising
 
          
 A=diag(lambda); %make a diagonal matrix from the eigenvalues
 M=U*A/U;  
 
 
 %sometimes +-ok (sum=0.94), sometimes not at all
 %29/10/2012: ok for norm 1, not for norm 2
 %wanting sparse matrix: U sparse doesn't mean at all that M is sparse: 
 %density U=0.05 gives density M= 0.88; density U >=0.1 gives density M=1.
 %But if we manipulate M so that it becomes sparse, how to hold condition
 %of relaxation?