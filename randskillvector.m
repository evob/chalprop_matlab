function [s,e1,e2,maxrelc,e3,e4,V,A] = randskillvector(m,n,p)
%creates a random skillvector, based on normed randomchallenges which folow a
%power-law, a=2, b=1
% m= number of random challenges used
% n= length of vector
% p= p-norm used

%functions used:
% benefit

[M,B] = processmatrix3(n,1/2,1);
[V,A] = eig(M);                   
lambda= diag(A);
%M=A;                              % make it simpler:just diag matrix (pretty ugly written)
s=zeros(n,1);
maxben=0;
[C,I] = min(abs(lambda));  %find eigenvector corresp with smallest abs eigenvalue, result is V(:,I)
R=eye(n);
c=R(:,I);
for i=1:m
b=benefit(c,M);
if b>maxben
    maxben=b;
    maxrelc=c;
end
s=s+benefit(c,M)*c;
c= transpose(powerlaw(n,2,1));
c=c/norm(c,p);
end
s=s/norm(s,p);


hypo= ones(n,1)-abs(lambda); %our hypothese is that the skillvector is similar to 1-lambda
e1=transpose(hypo)*s/norm(hypo,p);   %cosin sim between s and our hypothese
e2=transpose(V(:,I))*s/norm(V(:,I),p); % cosin simil between eigenvector corresp with lowest eigenvalue and s

e3=transpose(V(:,I))*maxrelc/(norm(V(:,I),p)*norm(maxrelc,p)); %cosin simil between vector which max extract benefit and "smallest" eigenvector
e4=transpose(maxrelc)*s/norm(maxrelc,p); %cosin simil between vector which max extract benefit and s


%result for M diagonalmatrix,p=2:
%e1 and e4 are close to 1, thus there is similarity between our hypothese and the skillvector, and between the skillvector and the max relaxed challenge
%e2 and e3 ongeveer 0.30, thus not really a similarity between the
%eigenvector with smallest eigenvalue and skillvector or maximum relaxed
%challenge. 

%General matrix,p=2: idem;
%e2,e3= -0.05, -0.08