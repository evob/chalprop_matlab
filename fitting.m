processstyle=0;
sendstyle=1;
Fit=[];
        for punishstyle=0:1
            for believe=0:0.1:1
                for relax=0:0.1:1
                    for percsent=0:0.1:1

load(['workspace',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)],'-mat','totalbenefit'); 

ax=fittype('a*x');


 
f=fit( transpose(linspace(1,T,T)),totalbenefit,ax);
line=coeffvalues(f);

 g=fit( transpose(linspace(1,T,T)),totalbenefit,'poly1');
pol1=coeffvalues(g);

h=fit( transpose(linspace(1,T,T)),totalbenefit,'poly2');
pol2=coeffvalues(h);

quad=fittype('a*x^2+b*x');
k=fit( transpose(linspace(1,T,T)),totalbenefit,quad);
qu=coeffvalues(k);

Fit= [Fit;processstyle,sendstyle,punishstyle,believe,relax,percsent, max(totalbenefit),line,pol1,pol2,qu];

                    end
                end
            end
        end

% [y,v]=max(Fit(:,8))          %calc max increasing in fit, the value and
% the place (by a*x)