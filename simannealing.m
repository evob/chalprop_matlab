



% believe=0.2;               %percentage of agents getting god challenges in one iteration 0.2
% percneg=0.2;               %percentage of negative components in god challenges 
% lambda=0.5;          %learning rate of network, between 0 and 1   0.3
% kappa=0.2;           %reciprocity rate, perc of link adaption also dapted in other direction, between 0 and 1      0.2
% percsent=0.4;

%(believe,percneg,lambda,kappa,percsent)

x0=[0.2,0.2,0.5,0.2,0.4];
oldopts = saoptimset('simulannealbnd');
 options = saoptimset(oldopts,'MaxIter',1000);
[x,fval,exitflag,output] = simulannealbnd(@Chalpropbenincrease,x0,zeros(1,5),ones(1,5),options)