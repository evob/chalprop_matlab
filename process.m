function [newsituation,skillvector,benefit,memory,relax]= process(challenge,memory,P,need,skillvector,r,p)

%challenge=challenge to be processed
%memory=memory of a
%P= process matrix of a
%need=needvector of a
%skillvector=skillvector of a
%totalbenefit= totalbenefit of a. Not used, fitness of a is defined as sum
%of T next benefits
%r=number of rival componenentzzzzzzzz

%challenge= situation - need(a,:);

c=challenge(2:end);
if sum(challenge(1)==memory) ~= 0  %if challenge has allready passed, non-rival parts are ignored
    c=[c(1:r) zeros(1,length(c)-r)];
end


processed = P*transpose(c);
processed=transpose(processed);
   
benefitvector=abs(c)-abs(processed);
benefit=sum(benefitvector);
skillvector=skillvector+benefit*challenge(2:end);

%totalbenefit=totalbenefit+benefit;

relax= 1-norm(processed,p)/norm(c,p);
newsituation =challenge(2:end)+need;           %=old situation, only rival parts are changed
%for j=1:r %rival parts
    newsituation(1:r)= processed(1:r)+need(1:r);
%end

newsituation=[challenge(1) newsituation];
memory=[memory challenge(1)];  %put challenge in memory

    