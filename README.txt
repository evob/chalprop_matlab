Welcome to the Chalprop Repo
-------------------------------
This repo is the MATLAB code of the Challenge propagation model.

How to look at it:

Chalpropcore.m is the main code, a lot of the other functions are used here
runs.m runs the simulation and plots some variables
variablevariation.m and similar runs the simulation for changing variables
fitting.m and similar uses this data to put it in a matrix, with some fitting parameters of totalbenefit
plotagainst... makes a plot of the average benefit of one variable
findmax.m and similar finds the max scope from a certain matrix
(one matrix, Fit3, is also included here)

test... is to do some tests whether excepted properties hold