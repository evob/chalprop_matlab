% %Start by variating:
% processstyle=0;              %0 if the processmatrix is diagonalmatrix, 1 if it is a sparse matrix, 2/other if it is a full matrix 
% sendstyle=2;                 %0 if always send, 1 if with link weight as probability (and no pref function), 2 if with preference function, 3/other if with pref function and weight as prob
% punishstyle=0;                %0 if neg benefit is used to lower the weights in between, 1 if not
% believe                       % %percentage of agents getting god challenges in one iteration 0.2
% ws                            %weightstrong, the weight is powered with this value to compute priority, so that the weight has less influence if ws<1; only used if weight in prob (thus sendstyle 0 or 2)
% 
% relax=0.5;                 %factor by which a challenge on average is relaxed
% 
% percsent=0.4;              %percsent
% 
% %Next step: also
% lambda=0.3;          %learning rate of network, between 0 and 1
% kappa=0.2;           %reciprocity rate, perc of link adaption also dapted in other direction, between 0 and 1
% decay=0;               %rate for which links are weakened in each step (0.01?), rate of disepation  -Should be 0 if we renormalize
% 
% 
% %Then: everything (especially amount of non-zero's)

for processstyle=0:2
    for sendstyle=[1,3]
        for punishstyle=0:1
            for believe=0:0.1:1
                for relax=0:0.1:1
                    for percsent=0:0.1:1
                
ChalPropcorelessvar;
computeavrel;
computeavchalnorm;
plots;
%save workspace, with name a100, with 100 value of i
save(['workspace',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)])
%save figure(1), same name

print(figure(1), '-djpeg', ['avrelax',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);
print(figure(2), '-djpeg', ['chalintensity',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);
print(figure(3), '-djpeg', ['hypben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);
print(figure(4), '-djpeg', ['totalposben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);
print(figure(5), '-djpeg', ['totalben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);
print(figure(6), '-djpeg', ['avpriority',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent)]);

                    end
                end
            end
        end
    end

    for sendstyle=[0,2]
        for punishstyle=0:1
            for believe=0:0.1:1
                for ws=0:0.1:1
                for relax=0:0.1:1
                    for percsent=0:0.1:1
                
ChalPropcorelessvar;
computeavrel;
computeavchalnorm;
plots;
%save workspace, with name a100, with 100 value of i
save(['workspace',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)])
%save figure(1), same name

print(figure(1), '-djpeg', ['avrelax',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);
print(figure(2), '-djpeg', ['chalintensity',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);
print(figure(3), '-djpeg', ['hypben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);
print(figure(4), '-djpeg', ['totalposben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);
print(figure(5), '-djpeg', ['totalben',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);
print(figure(6), '-djpeg', ['avpriority',num2str(processstyle),',',num2str(sendstyle),',',num2str(punishstyle),',',num2str(believe),',',num2str(relax),',',num2str(percsent),',',num2str(ws)]);

                    end
                end
            end
        end
        end
    end
end
    

