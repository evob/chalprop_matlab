function [input,mark] = godrain(N,n,factor,cst,x,need,skillvector,input,mark,believe,p,percneg)

for a=1:n
    if rand(1)<believe
        sit=challengevector(N,factor,cst,x,percneg);
        c=sit-need(a,:);
        prio=priority(c,1,skillvector(a,:),p);
        input{a}=[input{a};prio -1 mark+a c];
        mark=mark+1;
    end
end
