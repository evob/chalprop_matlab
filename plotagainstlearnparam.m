a=2;   %whichparam: lambda or kappa
b=4;   %approxed slope
y=zeros(11,1);
count=zeros(11,1);
x=zeros(11,1);
c=1;
for k= 0:0.1:1
x(c)=k;
c=c+1;
end

for i=1:121
    for j=1:11
        if Fitlearn(i,a)==x(j)
            y(j)=y(j)+Fitlearn(i,b);
            count(j)=count(j)+1;
            break
        end
    end
end
y=y./count;
plot(x,y);
cd C:\Users\Evo\Dropbox\ECCO\MATLABcode\General-chooseoptions+moredata
%print(figure(1), '-djpeg', ['relaxax3']);
print(figure(1), '-djpeg', ['kappaax3']);
 cd C:\Users\Evo\Dropbox\ECCO\MATLABcode\General-chooseoptions+moredata\code