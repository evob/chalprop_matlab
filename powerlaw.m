function c= powerlaw(N,a,b,percneg)
%N=number of iterations/data
% frequency=power-law by bx^(-a)

c=zeros(1,N);
for i=1:N
x = rand(1);
sgn=sign(rand(1)-percneg);
c(i) = sgn*((-a+1)*(x-1)/b)^(1/(-a+1));
end
%c=sort(c);
%hist(c,100);


